# Java SDK

Java SDK for the AirborneRF Aviation API.

## Using the Java SDK in your code



## Demo application

The repository comes with a demo application that uses the AirborneRF Aviation API and 
demonstrates its general usage. You can run it directly with `gradle`.

To use the demo, you need to have available:
* The API endpoint URL
* The API auth token

To run the demo:

```shell
# replace "endpoint-url" and "auth-token" with the values you were provided
gradle run --args="endpoint-url auth-token"
```
