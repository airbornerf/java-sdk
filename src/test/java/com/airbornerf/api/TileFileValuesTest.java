package com.airbornerf.api;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;

// Beware: https://stackoverflow.com/a/70251744
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import com.google.common.io.Resources;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TileFileValuesTest {

	@TempDir
	Path tmpDir;

	private static class TileValuesTestCase {
		public final String tilePath;
		public final String expectedValuesPath;
		public final String expectedTileDimPath;

		public TileValuesTestCase(String tilePath, String expectedValuesPath, String expectedTileDimPath) {
			this.tilePath = tilePath;
			this.expectedValuesPath = expectedValuesPath;
			this.expectedTileDimPath = expectedTileDimPath;
		}
	}

	private static class TileDim {
		public final int xsize;
		public final int ysize;
		public final int zsize;

		public TileDim(int xsize, int ysize, int zsize) {
			this.xsize = xsize;
			this.ysize = ysize;
			this.zsize = zsize;
		}
	}

	@Test
	public void testTileFileValues() throws Exception
	{
		List<TileValuesTestCase> testCases = getTileTestCases();

		for (TileValuesTestCase testCase : testCases) {

			TileDim expectedTileDimensions = parseExpectedTileDimensions(testCase.expectedTileDimPath);
			Integer[][][] expectedValues = parseExpectedValues(testCase.expectedValuesPath, expectedTileDimensions);

			Tile tile = new Tile(new File(testCase.tilePath));

			assertEquals(tile.getHeader().xsize, expectedTileDimensions.xsize);
			assertEquals(tile.getHeader().ysize, expectedTileDimensions.ysize);
			assertEquals(tile.getHeader().zsize, expectedTileDimensions.zsize);

			for (int x = 0; x < tile.getHeader().xsize; x++) {
				for (int y = 0; y < tile.getHeader().ysize; y++) {
					for (int z = 0; z < tile.getHeader().zsize; z++) {
						assertEquals(expectedValues[x][y][z], Integer.valueOf(tile.getNode(x, y, z)));
					}
				}
			}
		}
	}

	private List<TileValuesTestCase> getTileTestCases() throws IOException {
		String tileTestCasesTar = Resources.getResource("tiletestcases.tar.gz").getFile();
		String tileTestCasesDir = Path.of(tmpDir.toString(), "tiletestcases").toString();

		createDirectory(tileTestCasesDir);
		extractTar(tileTestCasesTar, tileTestCasesDir);
		String testDataPath = Path.of(tileTestCasesDir, "tiletestcases").toString();

		List<TileValuesTestCase> testCases = new ArrayList<>();

		Files.list(Path.of(testDataPath))
			.filter(Files::isDirectory)
			.forEach(testCasePath -> {
				String expectedValuesPath = Path.of(testCasePath.toString(), "expected_values.csv").toString();
				Path tilePathGZipped = Path.of(testCasePath.toString(), "test_tile.tile.gz");
				Path tilePath = Path.of(testCasePath.toString(), "test_tile.tile");

				try
				{
					decompressGzip(tilePathGZipped, tilePath);
				}
				catch (IOException e)
				{
				}

				String expectedTileDimPath = Path.of(testCasePath.toString(), "expected_tile_dim.json").toString();

				testCases.add(new TileValuesTestCase(tilePath.toString(), expectedValuesPath, expectedTileDimPath));
			});

		assertEquals(96, testCases.size());

		return testCases;
	}

	private TileDim parseExpectedTileDimensions(String expectedTileDimPath) throws IOException
	{
		try (BufferedReader br = new BufferedReader(new FileReader(expectedTileDimPath))) {
			String json = br.lines().collect(Collectors.joining());
			JsonObject tileDim = new JsonParser().parse(json).getAsJsonObject();
			int xsize = tileDim.get("xsize").getAsInt();
			int ysize = tileDim.get("ysize").getAsInt();
			int zsize = tileDim.get("zsize").getAsInt();

			return new TileDim(xsize, ysize, zsize);
		}
	}

	private Integer[][][] parseExpectedValues(String expectedValuesPath, TileDim expectedTileDimensions) throws IOException {

		int xsize = expectedTileDimensions.xsize;
		int ysize = expectedTileDimensions.ysize;
		int zsize = expectedTileDimensions.zsize;

		Integer[][][] result = new Integer[xsize][ysize][zsize];

		try (BufferedReader br = new BufferedReader(new FileReader(expectedValuesPath))) {
			String line;
			int z = 0;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				assert values.length == xsize * ysize;
				for (int y = 0; y < ysize; y++) {
					for (int x = 0; x < xsize; x++) {
						result[x][y][z] = Integer.parseInt(values[y * xsize + x]);
					}
				}
				z++;
			}
			assertEquals(z, zsize);
		}

		return result;
	}

	private void extractTar(String sourcePath, String targetPath) throws IOException {
		try (TarArchiveInputStream tarIn = new TarArchiveInputStream(new GZIPInputStream(new FileInputStream(sourcePath)))) {
			TarArchiveEntry tarEntry;
			while ((tarEntry = tarIn.getNextTarEntry()) != null) {
				if (!tarEntry.isDirectory()) {
					String filePath = Path.of(targetPath, tarEntry.getName()).toString();
					createDirectory(Path.of(filePath).getParent().toString());
					Files.copy(tarIn, Path.of(filePath), StandardCopyOption.REPLACE_EXISTING);
				}
			}
		}
	}

	private static void decompressGzip(Path source, Path target) throws IOException {

		try (GZIPInputStream gis = new GZIPInputStream(
			new FileInputStream(source.toFile()));
		     FileOutputStream fos = new FileOutputStream(target.toFile())) {

			// copy GZIPInputStream to FileOutputStream
			byte[] buffer = new byte[1024];
			int len;
			while ((len = gis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
		}
	}

	private void createDirectory(String directoryPath) throws IOException {
		Files.createDirectories(Path.of(directoryPath));
	}
}
