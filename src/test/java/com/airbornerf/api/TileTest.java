package com.airbornerf.api;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;
import com.google.common.io.Resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.GZIPInputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TileTest
{
	@Test
	public void testTilespec()
	{
		assertEquals("n475e0165", Tile.generateTilespec(47.5, 16.5));
		assertEquals("s300e0145", Tile.generateTilespec(-30, 14.5));

		assertEquals("n000e0000", Tile.generateTilespec(0.0,          0.0));
		assertEquals("n475e0165", Tile.generateTilespec(47.51,      16.51));
		assertEquals("n470e0160", Tile.generateTilespec(47.0,        16.0));
		assertEquals("n469e0160", Tile.generateTilespec(46.91,        16.0));
		assertEquals("s300e0145", Tile.generateTilespec(-30.0,        14.5));
		assertEquals("s301e0145", Tile.generateTilespec(-30.091,        14.5));
		assertEquals("s302e0145", Tile.generateTilespec(-30.11,        14.5));
		assertEquals("n381w0785", Tile.generateTilespec(38.11,       -78.49));
		assertEquals("s380w0785", Tile.generateTilespec(-38.0,       -78.49));
		assertEquals("s380w0785", Tile.generateTilespec(-37.91,       -78.49));
		assertEquals("s382w0785", Tile.generateTilespec(-38.11,       -78.49));
		assertEquals("n380w0785", Tile.generateTilespec(38.0,       -78.49));
		assertEquals("n380w0790", Tile.generateTilespec(38.0,       -79.0));
		assertEquals("n380w0790", Tile.generateTilespec(38.09,       -79.0));
		assertEquals("n380w0790", Tile.generateTilespec(38.0, -78.91)); // ok
		assertEquals("s851w1800", Tile.generateTilespec(-85.000397, -180.0));
		assertEquals("n399w0780", Tile.generateTilespec(39.95, -77.95));  // ok
		assertEquals("n399w0779", Tile.generateTilespec(39.95, -77.85));  // ok
		assertEquals("n399w0778", Tile.generateTilespec(39.95, -77.75));  // ok
		assertEquals("n399w0777", Tile.generateTilespec(39.95, -77.65));  // ok
		assertEquals("n399w0776", Tile.generateTilespec(39.95, -77.55));  // ok
		assertEquals("n399w0775", Tile.generateTilespec(39.95, -77.45));  // ok
		assertEquals("n399w0774", Tile.generateTilespec(39.95, -77.35));  // ok
		assertEquals("n399w0773", Tile.generateTilespec(39.95, -77.25));  // ok
		assertEquals("n399w0772", Tile.generateTilespec(39.95, -77.15));  // ok
		assertEquals("n399w0771", Tile.generateTilespec(39.95, -77.05));  // ok
	}

	@Test
	public void testTilespec1Deg()
	{
		assertEquals("n00e000", Tile.generateTilespec1Deg(0, 0));

		assertEquals("n00e000", Tile.generateTilespec1Deg(0.1, 0.1));
		assertEquals("s01e000", Tile.generateTilespec1Deg(-0.1, 0.1));
		assertEquals("s01w001", Tile.generateTilespec1Deg(-0.1, -0.1));
		assertEquals("n00w001", Tile.generateTilespec1Deg(0.1, -0.1));

		assertEquals("n01e001", Tile.generateTilespec1Deg(1, 1));
		assertEquals("s01e001", Tile.generateTilespec1Deg(-1, 1));
		assertEquals("s01w001", Tile.generateTilespec1Deg(-1, -1));
		assertEquals("n01w001", Tile.generateTilespec1Deg(1, -1));

		assertEquals("n47e016", Tile.generateTilespec1Deg(47.5, 16.5));
		assertEquals("s30e014", Tile.generateTilespec1Deg(-30, 14.5));
		assertEquals("n00e000", Tile.generateTilespec1Deg(0.0, 0.0));
		assertEquals("n47e016", Tile.generateTilespec1Deg(47.51, 16.51));
		assertEquals("n47e016", Tile.generateTilespec1Deg(47.0, 16.0));
		assertEquals("n46e016", Tile.generateTilespec1Deg(46.91, 16.0));
		assertEquals("s30e014", Tile.generateTilespec1Deg(-30.0, 14.5));
		assertEquals("s31e014", Tile.generateTilespec1Deg(-30.091, 14.5));
		assertEquals("s31e014", Tile.generateTilespec1Deg(-30.11, 14.5));
		assertEquals("n38w079", Tile.generateTilespec1Deg(38.11, -78.49));
		assertEquals("s38w079", Tile.generateTilespec1Deg(-38.0, -78.49));
		assertEquals("s38w079", Tile.generateTilespec1Deg(-37.91, -78.49));
		assertEquals("s39w079", Tile.generateTilespec1Deg(-38.11, -78.49));
		assertEquals("n38w079", Tile.generateTilespec1Deg(38.0, -78.49));
		assertEquals("n38w079", Tile.generateTilespec1Deg(38.0, -79.0));
		assertEquals("n38w079", Tile.generateTilespec1Deg(38.09, -79.0));
		assertEquals("n38w079", Tile.generateTilespec1Deg(38.0, -78.91)); // ok
		assertEquals("s86w180", Tile.generateTilespec1Deg(-85.000397, -180.0));
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.95));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.85));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.75));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.65));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.55));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.45));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.35));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.25));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.15));  // ok
		assertEquals("n39w078", Tile.generateTilespec1Deg(39.95, -77.05));  // ok
	}

	private File makeTilefileAvailableForTesting(String name) throws Exception
	{
		File tile = null;
		try
		{
			String path = Resources.getResource(name).getFile();
			tile = new File(path);
		}
		catch(IllegalArgumentException ex)
		{
			// File does not exist - unzip it from the .gz file
			String path = Resources.getResource(name + ".gz").getFile();
			String outPath = path.substring(0, path.length() - 3);

			byte[] buffer = new byte[1024];
			GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(path));
			FileOutputStream out = new FileOutputStream(outPath);

			int len;
			while ((len = gzis.read(buffer)) > 0)
			{
				out.write(buffer, 0, len);
			}

			gzis.close();
			out.close();

			tile = new File(outPath);
		}
		assertTrue(tile.exists());
		return tile;
	}

	private File makeTilefileAvailableForTestingBZ2(String name) throws Exception
	{
		File tile = null;
		try
		{
			String path = Resources.getResource(name).getFile();
			tile = new File(path);
		}
		catch(IllegalArgumentException ex)
		{
			// File does not exist - unzip it from the .gz file
			String path = Resources.getResource(name + ".bz2").getFile();
			String outPath = path.substring(0, path.length() - 3);

			byte[] buffer = new byte[1024];
			BZip2CompressorInputStream gzis = new BZip2CompressorInputStream(new FileInputStream(path));
			FileOutputStream out = new FileOutputStream(outPath);

			int len;
			while ((len = gzis.read(buffer)) > 0)
			{
				out.write(buffer, 0, len);
			}

			gzis.close();
			out.close();

			tile = new File(outPath);
		}
		assertTrue(tile.exists());
		return tile;
	}

	public void assertNear(int a, int b, int range)
	{
		assertTrue((b - range) <= a && a <= (b + range));
	}

	@Test
	public void testReadingV4() throws Exception
	{
		// Open a version 4 elevation tile.
		File tileFile = makeTilefileAvailableForTestingBZ2("n47e016_elevation.tile.v4");
		Tile tile = new Tile(tileFile);

		assertTrue(tile.validateChecksum());

		assertEquals(tile.getHeader().xsize, 3600);
		assertEquals(tile.getHeader().ysize, 3600);
		assertEquals(tile.getHeader().zsize, 1);

		// Check some heights
		Pair<Integer, Integer> xy = tile.convertToTileCoordinates(47.843099f, 16.259886f);
		assertEquals(tile.getNode(xy.getLeft(), xy.getRight(), 0), 265);

		xy = tile.convertToTileCoordinates(47.661540f, 16.277572f);
		assertEquals(tile.getNode(xy.getLeft(), xy.getRight(), 0), 629);

		xy = tile.convertToTileCoordinates(47.348019f, 16.412280f);
		assertEquals(tile.getNode(xy.getLeft(), xy.getRight(), 0), 812);

		xy = tile.convertToTileCoordinates(47.855793f, 16.769651f);
		assertEquals(tile.getNode(xy.getLeft(), xy.getRight(), 0), 110);
	}

	@Test
	public void testReadingV4Binary() throws Exception
	{
		// Open a version 4 safezone tile.
		File tileFile = makeTilefileAvailableForTestingBZ2("n47e016_safezone.tile.v4");
		Tile tile = new Tile(tileFile);

		assertTrue(tile.validateChecksum());

		assertEquals(tile.getHeader().xsize, 1800);
		assertEquals(tile.getHeader().ysize, 1800);
		assertEquals(tile.getHeader().zsize, 51);

		// Iterate through the whole tile
		float latMin = 90;
		float latMax = 0;
		float lonMin = 180;
		float lonMax = 0;
		int altMin = 10000;
		int altMax = 0;

		for(int z = 0; z < tile.getHeader().zsize; z++)
		{
			for(int y = 0; y < tile.getHeader().ysize; y++)
			{
				for(int x = 0; x < tile.getHeader().xsize; x++)
				{
					// Read the value (0 or 1) at position x, y, z
					int value = tile.getNode(x, y, z);

					// If you want to know the real world coordinates of x, y, z, convert like this:
					Triple<Float, Float, Integer> coords = tile.getLatLongAlt(x, y, z);
					float latitude = coords.getLeft();
					float longitude = coords.getMiddle();
					int altitude = coords.getRight();

					// Likewise, if you have latitude, longitude and altitude, and you want to read
					// the tile value, you need to convert it to x, y, z tile coordinates first:
					Tile.PointXYZ xyz = tile.convertToTileCoordinates(latitude, longitude, altitude);

					assertEquals(xyz.x, x);
					assertEquals(xyz.y, y);
					assertEquals(xyz.z, z);

					if(latitude < latMin)
						latMin = latitude;
					if(latitude > latMax)
						latMax = latitude;
					if(longitude < lonMin)
						lonMin = longitude;
					if(longitude > lonMax)
						lonMax = longitude;
					if(altitude < altMin)
						altMin = altitude;
					if(altitude > altMax)
						altMax = altitude;
				}
			}
		}

		assertEquals(latMin, 47.0, 0.000001);
		assertEquals(latMax, 47.999443, 0.000001);
		assertEquals(lonMin, 16.0, 0.000001);
		assertEquals(lonMax, 16.999445, 0.000001);
		assertEquals(altMin, 90);
		assertEquals(altMax, 1590);
	}
}
