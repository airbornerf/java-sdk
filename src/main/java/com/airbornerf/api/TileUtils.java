/*
 * Copyright (c) 2019 Dimetor GmbH.
 *
 * NOTICE: All information contained herein is, and remains the property
 * of Dimetor GmbH and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Dimetor GmbH and its
 * suppliers and may be covered by European and Foreign Patents, patents
 * in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Dimetor GmbH.
 */

package com.airbornerf.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static java.lang.Math.*;
import static java.lang.Math.floor;
import static java.lang.Math.max;
import static java.lang.Math.min;

public class TileUtils
{
	/**
	 * Calculate the list of tilespecs that are covered with the given coordinates.
	 *
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return
	 */
	public static List<String> getTilespecsInArea(double lat1, double lon1, double lat2, double lon2)
	{
		double latMin = min(lat1, lat2);
		double latMax = max(lat1, lat2);
		double lonMin = min(lon1, lon2);
		double lonMax = max(lon1, lon2);

		Location2D<Double> sw = normalizeCoordinates(latMin, lonMin);
		Location2D<Double> ne = normalizeCoordinates(latMax, lonMax);

		// Round to the 0.1-degree-grid-lines.
		int deciLatMin = (int) floor(sw.latitude*10);
		int deciLonMin = (int) floor(sw.longitude*10);
		int deciLatMax = (int) ceil(ne.latitude*10);
		int deciLonMax = (int) ceil(ne.longitude*10);

		List<String> tilespecs = new ArrayList<String>();
		for (int deci_lat = deciLatMin; deci_lat < deciLatMax; deci_lat += 1)
		{
			for (int deci_lon = deciLonMin; deci_lon < deciLonMax; deci_lon += 1)
			{
				tilespecs.add(Tile.generateTilespec(deci_lat/10.0 + 0.05, deci_lon/10.0 + 0.05));
			}
		}

		return tilespecs;
	}

	public static List<String> getTilespecs1DegInArea(double lat1, double lon1, double lat2, double lon2)
	{
		double latMin = min(lat1, lat2);
		double latMax = max(lat1, lat2);
		double lonMin = min(lon1, lon2);
		double lonMax = max(lon1, lon2);

		Location2D<Double> sw = normalizeCoordinates(latMin, lonMin);
		Location2D<Double> ne = normalizeCoordinates(latMax, lonMax);

		// Round to the 0.1-degree-grid-lines.
		int latMinInt = (int) floor(sw.latitude);
		int lonMinInt = (int) floor(sw.longitude);
		int latMaxInt = (int) ceil(ne.latitude);
		int lonMaxInt = (int) ceil(ne.longitude);

		List<String> tilespecs = new ArrayList<String>();
		for (int lat = latMinInt; lat < latMaxInt; lat += 1)
		{
			for (int lon = lonMinInt; lon < lonMaxInt; lon += 1)
			{
				tilespecs.add(Tile.generateTilespec1Deg(lat + 0.5, lon+0.5));
			}
		}

		return tilespecs;
	}

	public static class Location2D<T extends Number>
	{
		private T latitude;
		private T longitude;

		public Location2D()
		{
		}

		public Location2D(T latitude, T longitude)
		{
			this.latitude = latitude;
			this.longitude = longitude;
		}

		public T getLatitude()
		{
			return latitude;
		}

		public void setLatitude(T latitude)
		{
			this.latitude = latitude;
		}

		public T getLongitude()
		{
			return longitude;
		}

		public void setLongitude(T longitude)
		{
			this.longitude = longitude;
		}
	}

	/**
	 * Returns the point-of-reference for the given tilespec,
	 * which is southwestern edge of the tile (the southwestern edge of the most southwestern pixel of the tile).
	 *
	 * @param tilespec
	 * @return
	 */
	public static Location2D<Float> getCoordinatesForTilespec(String tilespec)
	{
		if(tilespec.length() == 9)
		{
			return getCoordinatesForCentiTilespec(tilespec);
		}
		else if (tilespec.length() == 7)
		{
			return getCoordinatesForOneDegreeTilespec(tilespec);
		}
		else
		{
			throw new RuntimeException("Cannot process tilespec '" + tilespec + "'.");
		}
	}

	private static Location2D<Float> getCoordinatesForCentiTilespec(String tilespec)
	{
		Location2D<Float> res = new Location2D();

		res.latitude = (tilespec.charAt(1) - '0') * 10 + (tilespec.charAt(2) - '0') + (tilespec.charAt(3) - '0') * 0.1f;

		if (tilespec.charAt(0) == 's' || tilespec.charAt(0) == 'S') {
			res.latitude *= -1;
		}

		res.longitude = (tilespec.charAt(5) - '0') * 100 + (tilespec.charAt(6) - '0') * 10 + (tilespec.charAt(7) - '0') + (tilespec.charAt(8) - '0') * 0.1f;

		if (tilespec.charAt(4) == 'w' || tilespec.charAt(4) == 'W') {
			res.longitude *= -1;
		}

		return res;
	}

	private static Location2D<Float> getCoordinatesForOneDegreeTilespec(String tilespec)
	{
		Location2D<Float> res = new Location2D();

		res.latitude = (float) (tilespec.charAt(1) - '0') * 10 + (tilespec.charAt(2) - '0');

		if (tilespec.charAt(0) == 's' || tilespec.charAt(0) == 'S') {
			res.latitude *= -1;
		}

		res.longitude = (float) (tilespec.charAt(4) - '0') * 100 + (tilespec.charAt(5) - '0') * 10 + (tilespec.charAt(6) - '0');

		if (tilespec.charAt(3) == 'w' || tilespec.charAt(3) == 'W') {
			res.longitude *= -1;
		}

		return res;
	}

	/**
	 * Calculates a bounding rectangle for a list of tilespecs.
	 *
	 * @param tilespecs
	 * @return
	 */
	public static Rectangle getAreaForTilespecs(List<String> tilespecs)
	{
		Rectangle rect = null;
		for (String tilespec : tilespecs)
		{
			if (tilespec.matches("[nNsS]\\d\\d\\d[eEwW]\\d\\d\\d\\d"))
			{
				Location2D<Float> southwest = getCoordinatesForTilespec(tilespec);

				float west = southwest.longitude;
				float south = southwest.latitude;
				float east = southwest.longitude + 0.1f;
				float north = southwest.latitude + 0.1f;

				if (rect == null)
				{
					rect = new Rectangle(west, south, east, north);
				}
				else
				{
					rect.west = min(rect.west, southwest.longitude);
					rect.south = min(rect.south, southwest.latitude);
					rect.east = max(rect.east, southwest.longitude + 0.1f);
					rect.north = max(rect.north, southwest.latitude + 0.1f);
				}
			}
		}
		return rect;
	}

	public static Rectangle getAreaTilespecs1Deg(List<String> tilespecs)
	{
		Rectangle rect = null;
		for (String tilespec : tilespecs)
		{
			if (tilespec.matches("[nNsS]\\d\\d[eEwW]\\d\\d\\d"))
			{
				Location2D<Float> southwest = getCoordinatesForTilespec(tilespec);

				int west = southwest.longitude.intValue();
				int south = southwest.latitude.intValue();
				int east = west + 1;
				int north = south + 1;

				if (rect == null)
				{
					rect = new Rectangle(west, south, east, north);
				}
				else
				{
					rect.west = min(rect.west, west);
					rect.south = min(rect.south, south);
					rect.east = max(rect.east, east);
					rect.north = max(rect.north, north);
				}
			}
		}
		return rect;
	}

	public static Location2D<Double> normalizeCoordinates(double latitude, double longitude)
	{
		/*
		 * Fix up input lat/lon to be within [-90,90] and [-180,180) respectively.
		 * This avoids problems if out of range coordinates are given anywhere (e.g.
		 * as user input).
		 *
		 * Longitude wraps around at 180 to -180. The reason for choosing this range
		 * (instead of (-180,180]) is that the tilespec represents the SW corner, so
		 * the tile comprises points "to the right" of its name - this way they have
		 * the same sign.
		 *
		 * Latitudes >90 and <-90 are treated as being mirrored at +-90, with the
		 * longitude being "reversed" in this case - as if one walked straight over
		 * the north or south pole.
		 */

		if(latitude >= 180 || latitude < -180)
		{
			latitude = ((latitude + 180) % 360 + 360) % 360 - 180;
		}

		if(latitude >= 90 )
		{
			latitude = 180 - latitude;
			longitude += 180;
		}
		else if (latitude < -90)
		{
			latitude = -180 - latitude;
			longitude += 180;
		}
		if(longitude >= 180 || longitude < -180)
		{
			longitude = ((longitude + 180) % 360 + 360) % 360 - 180;
		}

		return new Location2D<>(latitude, longitude);
	}
}
