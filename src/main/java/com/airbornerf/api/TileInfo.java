/*
 * Copyright (c) 2019 Dimetor GmbH.
 *
 * NOTICE: All information contained herein is, and remains the property
 * of Dimetor GmbH and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Dimetor GmbH and its
 * suppliers and may be covered by European and Foreign Patents, patents
 * in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Dimetor GmbH.
 */

package com.airbornerf.api;

public class TileInfo
{

	private String tileSpec;
	private float latitudeSouthWest;
	private float longitudeSouthWest;

	public TileInfo(String tileSpec, float latitudeSouthWest, float longitudeSouthWest)
	{
		this.tileSpec = tileSpec;
		this.latitudeSouthWest = latitudeSouthWest;
		this.longitudeSouthWest = longitudeSouthWest;
	}

	public String getTileSpec()
	{
		return tileSpec;
	}

	public float getLatitudeSouthWest()
	{
		return latitudeSouthWest;
	}

	public float getLongitudeSouthWest()
	{
		return longitudeSouthWest;
	}
}