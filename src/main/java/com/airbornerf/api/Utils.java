package com.airbornerf.api;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.lz4.FramedLZ4CompressorInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Utils
{
    public static void ungzip(File infile, File outfile) throws Exception
    {
        byte[] buffer = new byte[1024];
        GzipCompressorInputStream gzis = new GzipCompressorInputStream(new FileInputStream(infile));
        FileOutputStream out = new FileOutputStream(outfile);

        int len;
        while ((len = gzis.read(buffer)) > 0)
        {
            out.write(buffer, 0, len);
        }

        gzis.close();
        out.close();
    }

    public static void unlz4(File infile, File outfile) throws Exception
    {
        byte[] buffer = new byte[1024];
        FramedLZ4CompressorInputStream lz4is = new FramedLZ4CompressorInputStream(new FileInputStream(infile));
        FileOutputStream out = new FileOutputStream(outfile);

        int len;
        while ((len = lz4is.read(buffer)) > 0)
        {
            out.write(buffer, 0, len);
        }

        lz4is.close();
        out.close();
    }
}
