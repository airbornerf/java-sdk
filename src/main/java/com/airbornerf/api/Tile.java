/*
 * Copyright (c) 2018 Dimetor GmbH.
 *
 * NOTICE: All information contained herein is, and remains the property
 * of Dimetor GmbH and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Dimetor GmbH and its
 * suppliers and may be covered by European and Foreign Patents, patents
 * in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Dimetor GmbH.
 */

package com.airbornerf.api;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Arrays;

public class Tile
{
	static class TileConstants
	{
		public static final int HEADER_OFFSET = 582; // size of the header.
		public static final int PADDING_SIZE = 512; // unused part of the header.
		public static final int ACTUAL_HEADER_SIZE = 582 - 512;
	}

	private static final Logger log = LoggerFactory.getLogger(Tile.class);

	public class TileHeader
	{
		String magic;
		int version;
		int xsize, ysize, zsize;
		int altitude;
		int verticalResolution;
		float latitude, longitude;
		float latSize, lonSize;
		int dtype;
		int valueOffset;
		Instant creationTime;
		int ceiling;
		byte[] checksum;
	}

	public class CalculatedTileHeader
	{
		int horizontalLayerSize;
		long tileBufferLength;
	}


	public class PointXYZ
	{
		public int x;
		public int y;
		public int z;

		@Override
		public String toString()
		{
			return "x=" + x + ",y=" + y + ",z=" + z;
		}
	}

	private MappedByteBuffer buffer = null;
	private final TileHeader header;
	private final CalculatedTileHeader calculatedHeader;
	private int getNodeIndex( int x, int y, int z)
	{
		return z * calculatedHeader.horizontalLayerSize + y * header.xsize + x;
	}

	public Tile(File tileFile) throws Exception
	{
		RandomAccessFile rfp = new RandomAccessFile(tileFile.getAbsolutePath(), "r");

		FileChannel fileChannel = rfp.getChannel();
		long fileSize = fileChannel.size();
		buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileSize);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		header = new TileHeader();
		byte[] buf = new byte[TileConstants.ACTUAL_HEADER_SIZE];
		buffer.get(buf, 0, 8);
		header.magic = new String(buf, 0, 8);
		if (!header.magic.equals("DMTRTILE"))
		{
			throw new Exception("File is not a Dimetor Tile file!");
		}
		header.version = buffer.get();
		if (header.version != 4)
		{
			throw new Exception("Tile file version not supported!");
		}
		header.xsize = buffer.getInt();
		header.ysize = buffer.getInt();
		header.zsize = buffer.getInt();
		header.altitude = buffer.getShort();
		header.verticalResolution = buffer.getShort();
		header.latitude = buffer.getFloat();
		header.longitude = buffer.getFloat();
		header.latSize = buffer.getFloat();
		header.lonSize = buffer.getFloat();
		header.dtype = buffer.get();
		header.valueOffset = buffer.getShort();
		header.creationTime = Instant.ofEpochSecond(buffer.getInt());
		header.ceiling = buffer.getShort();
		header.checksum = new byte[20];
		buffer.get(header.checksum, 0, 20);

		// slurp the rest ( why ? )
		byte[] slurp = new byte[TileConstants.PADDING_SIZE];
		buffer.get(slurp, 0, TileConstants.PADDING_SIZE);

		log.debug("version = " + header.version);
		log.debug("xsize = " + header.xsize);
		log.debug("ysize = " + header.ysize);
		log.debug("zsize = " + header.zsize);
		log.debug("altitude = " + header.altitude);
		log.debug("verticalResolution = " + header.verticalResolution);
		log.debug("latitude = " + header.latitude);
		log.debug("longitude = " + header.longitude);
		log.debug("latSize = " + header.latSize);
		log.debug("lonSize = " + header.lonSize);
		log.debug("dtype = " + header.dtype);
		log.debug("valueOffset = " + header.valueOffset);
		log.debug("creationTime = " + header.creationTime);
		log.debug("ceiling = " + header.ceiling);

		if (header.dtype != 1 && header.dtype != 4 && header.dtype != 8 && header.dtype != 16)
		{
			throw new Exception("Only node dtypes of 1, 4, 8 or 16 are currently supported");
		}

		calculatedHeader = new CalculatedTileHeader();

		calculatedHeader.horizontalLayerSize = header.xsize * header.ysize;
		calculatedHeader.tileBufferLength = fileSize;
	}

	public TileHeader getHeader()
	{
		return header;
	}

	public CalculatedTileHeader getCalculatedHeader()
	{
		return calculatedHeader;
	}

	/**
	 * Generates the tilespec for the tile at latitude, longitude.
	 *
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	public static String generateTilespec(double latitude, double longitude)
	{
		return generateTileInfo(latitude, longitude).getTileSpec();
	}

	public static String generateTilespec1Deg(double latitude, double longitude)
	{
		return generateTileInfo1Deg(latitude, longitude).getTileSpec();
	}
	/**
	 * Returns the tileInfo for the given coordinates
	 *
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	public static TileInfo generateTileInfo(double latitude, double longitude)
	{
		TileUtils.Location2D<Double> normalizedCoordinates =  TileUtils.normalizeCoordinates(latitude, longitude);
		latitude = normalizedCoordinates.getLatitude();
		longitude = normalizedCoordinates.getLongitude();

		assert -90 <= latitude && latitude < 90 : "Latitude out of bounds";
		assert -180 <= longitude && longitude < 180 : "Longitude out of bounds";

		char[] tilespec = new char[9];

		tilespec[0] = latitude >= 0 ? 'n' : 's';
		tilespec[4] = longitude >= 0 ? 'e' : 'w';

		int lat_deci_deg = (int) (Math.abs(Math.floor(latitude * 10)));
		int q = lat_deci_deg / 100;
		int r = lat_deci_deg % 100;

		tilespec[1] = (char) (q + '0');
		q = r / 10;
		r = r % 10;
		tilespec[2] = (char) (q + '0');
		tilespec[3] = (char) (r + '0');

		int lon_deci_deg = (int) (Math.abs(Math.floor(longitude * 10)));
		q = lon_deci_deg / 1000;
		r = lon_deci_deg % 1000;

		tilespec[5] = (char) (q + '0');

		q = r / 100;
		r = r % 100;

		tilespec[6] = (char) (q + '0');

		q = r / 10;
		r = r % 10;

		tilespec[7] = (char) (q + '0');
		tilespec[8] = (char) (r + '0');

		float latitudeSouth = lat_deci_deg/10.f * (tilespec[0] == 'n' ? 1 : -1);
		float longitudeWest = lon_deci_deg/10.f * (tilespec[4] == 'e' ? 1 : -1);

		return new TileInfo(new String(tilespec), latitudeSouth, longitudeWest);
	}

	public static TileInfo generateTileInfo1Deg(double latitude, double longitude)
	{
		TileUtils.Location2D<Double> normalizedCoordinates =  TileUtils.normalizeCoordinates(latitude, longitude);
		latitude = normalizedCoordinates.getLatitude();
		longitude = normalizedCoordinates.getLongitude();

		assert -90 <= latitude && latitude < 90 : "Latitude out of bounds";
		assert -180 <= longitude && longitude < 180 : "Longitude out of bounds";

		char[] tilespec = new char[7];

		int lat = (int) (Math.abs(Math.floor(latitude)));

		tilespec[0] = latitude >= 0 ? 'n' : 's';
		tilespec[1] = (char) ((lat/10) + '0');
		tilespec[2] = (char) ((lat)%10 + '0');

		int lon = (int) (Math.abs(Math.floor(longitude)));

		tilespec[3] = longitude >= 0 ? 'e' : 'w';
		tilespec[4] = (char) ((lon/100) + '0');
		tilespec[5] = (char) ((lon/10)%10 + '0');
		tilespec[6] = (char) ((lon)%10 + '0');

		return new TileInfo(new String(tilespec), lat, lon);
	}

	public int getNode(int x, int y, int z) throws Exception
	{
		if (x >= header.xsize || y >= header.ysize || z >= header.zsize || x < 0 || y < 0 || z < 0)
		{
			return 1;
		}

		int nodeIndex = getNodeIndex(x, y, z);

		if (header.dtype == 1)
		{
			int byteIndex = nodeIndex / 8;
			int bitIndex = nodeIndex % 8;
			byteIndex += TileConstants.HEADER_OFFSET;
			return (buffer.get(byteIndex) & (1 << bitIndex)) >> bitIndex;
		}
		else if (header.dtype == 4)
		{
			int byteIndex = nodeIndex / 2;
			int nibbleIndex = nodeIndex % 2;
			byteIndex += TileConstants.HEADER_OFFSET;

			int bufVal = (buffer.get(byteIndex) & (0xF0 >> (nibbleIndex * 4))) >> ((1 - nibbleIndex) * 4);
			return bufVal == 0 ? Integer.MIN_VALUE : bufVal + header.valueOffset;
		}
		else if (header.dtype == 8)
		{
			int byteIndex = nodeIndex + TileConstants.HEADER_OFFSET;
			int bufVal = buffer.get(byteIndex);
			return bufVal == 0 ? Integer.MIN_VALUE : bufVal + header.valueOffset;
		}
		else if (header.dtype == 16)
		{
			int byteIndex = nodeIndex * 2 + TileConstants.HEADER_OFFSET;
			int bufVal = ((buffer.get(byteIndex) << 8) | (buffer.get(byteIndex + 1) & 0xFF)) & 0xFFFF;
			return bufVal == 0 ? Integer.MIN_VALUE : bufVal + header.valueOffset;
		}
		else
		{
			throw new Exception("Unsupported dtype " + String.valueOf(header.dtype));
		}
	}

	/**
	 * Find the z layer where the node value at the given coordinate is 0 for the first time.
	 *
	 * @param lat
	 * @param lon
	 */
	PointXYZ findCoordinates(double lat, double lon) throws Exception
	{
		PointXYZ rval = new PointXYZ();

		Pair<Integer, Integer> tileCoo2D = convertToTileCoordinates((float) lat, (float) lon);
		rval.x = tileCoo2D.getKey();
		rval.y = tileCoo2D.getValue();

		for (int myZ = 0; myZ < header.zsize; myZ++)
		{
			if (getNode(rval.x, rval.y, myZ) == 0)
			{
				rval.z = myZ;
				return rval;
			}
		}

		rval.x = -1;
		rval.y = -1;
		rval.z = -1;
		return rval;
	}

	/**
	 * Calculates the Z elevation at the given point.
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	int getZElevation(int x, int y) throws Exception
	{
		int zElevation = 0;

		for (int z = 0; z < header.zsize; z++)
		{
			if (getNode(x, y, z) == 0)
			{
				zElevation = z;
				break;
			}
		}

		return zElevation;
	}

	/**
	 * Returns elevation at the given point.
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	int getElevation(int x, int y) throws Exception
	{
		int zElevation = getZElevation(x, y);
		return zElevation * header.verticalResolution + header.altitude;
	}

	/**
	 * Converts the WGS 84 latitude and longitude values into integer tile coordinates.
	 * In other words, it assigns to a point the integer-valued (x,y)-tile-coordinates of the pixel on which the point is located,
	 * where we consider the pixels to be open at the eastern and southern boundaries to achieve a partition of the tile into pixels.
	 * The point is not located on the tile if and only if (x,y) is not contained in [0,1799] x [0,1799].
	 * In other words: Points which are located on the tile have tile coordinates which lie within the 2D-interval [0,1799] x [0,1799].
	 * @param lat
	 * @param lon
	 * @return
	 */
	public Pair<Integer, Integer> convertToTileCoordinates(float lat, float lon)
	{
		//Note: Java's round function rounds always up at 0.5, also for negative integers, which is the desired behaviour
		//So round(0.5) = 1, round(-0.5) = 1, round(-1.5) = -1
		int x = Math.round((lon - header.longitude) * header.xsize / header.lonSize);
		int y = Math.round((header.latitude - lat) * header.ysize / header.latSize);
		x = clamp(x, 0, header.xsize - 1);
		y = clamp(y, 0, header.ysize - 1);

		return Pair.of(x, y);
	}

	/**
	 * Converts the WGS 84 latitude and longitude values and an EGM 96 altitude value into integer tile coordinates.
	 * In other words, it assigns to a point the integer-valued (x,y,z)-tile-coordinates of the pixel on which the point is located,
	 * where we consider the pixels to be open at the eastern and southern boundaries to achieve a partition of the tile into pixels.
	 * The point is not located on the tile if and only if (x,y) is not contained in [0,1799] x [0,1799].
	 * In other words: Points which are located on the tile have tile coordinates which lie within the 2D-interval [0,1799] x [0,1799].
	 * @param lat
	 * @param lon
	 * @return
	 */
	public PointXYZ convertToTileCoordinates(float lat, float lon, float alt)
	{
		//Note: Java's round function rounds always up at 0.5, also for negative integers, which is the desired behaviour
		//So round(0.5) = 1, round(-0.5) = 1, round(-1.5) = -1
		PointXYZ rval = new PointXYZ();
		rval.x = Math.round((lon - header.longitude) * header.xsize / header.lonSize);
		rval.y = Math.round((header.latitude - lat) * header.ysize / header.latSize);
		rval.z = Math.round((alt - header.altitude) / ((float) header.verticalResolution));

		rval.x = clamp(rval.x, 0, header.xsize - 1);
		rval.y = clamp(rval.y, 0, header.ysize - 1);
		rval.z = clamp(rval.z, 0, header.zsize - 1);

		return rval;
	}

	/**
	 * Converts tile coordinates to real world coordinates. Points can be in between the tile indices.
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public Triple<Float, Float, Integer> getLatLongAlt(int x, int y, int z)
	{
		float lon = header.longitude + (float) x / (float) header.xsize * header.lonSize;
		float lat = header.latitude - (float) y / (float) header.ysize * header.latSize;
		int alt = Math.round((float)z * (float)header.verticalResolution + (float)header.altitude);
		return Triple.of(lat, lon, alt);
	}

	/**
	 * Checks if the checksum stored in the "checksum" header is correct for this tile.
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public boolean validateChecksum() throws NoSuchAlgorithmException
	{
		MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
		sha1.reset();
		buffer.rewind();

		// Update from the start of the buffer to the checksum
		byte[] buf = new byte[8192];
		buffer.get(buf, 0, 50);
		sha1.update(buf, 0, 50);

		// Next, add a fake checksum field with all zeros
		buffer.get(buf, 0, 20);
		for(int i = 0; i < 20; i++)
			sha1.update((byte) 0);

		// Finally, add everything from after the checksum to the end of file
		int idx = 70;
		while(idx < calculatedHeader.tileBufferLength)
		{
			int len = 8192;
			if (idx + len > calculatedHeader.tileBufferLength)
				len = (int)calculatedHeader.tileBufferLength - idx;
			buffer.get(buf, 0, len);
			sha1.update(buf, 0, len);
			idx += len;
		}

		return Arrays.equals(sha1.digest(), header.checksum);
	}

	private static int clamp(int value, int min_value, int max_value)
	{
		return Math.min(Math.max(min_value, value), max_value);
	}
}
