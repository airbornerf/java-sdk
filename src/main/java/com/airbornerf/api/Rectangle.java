/*
 * Copyright (c) 2019 Dimetor GmbH.
 *
 * NOTICE: All information contained herein is, and remains the property
 * of Dimetor GmbH and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Dimetor GmbH and its
 * suppliers and may be covered by European and Foreign Patents, patents
 * in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Dimetor GmbH.
 */

package com.airbornerf.api;

/**
 * Representation of a rectangular area with west, south, east and north coordinate in degrees.
 */
public class Rectangle
{
	public float west;
	public float south;
	public float east;
	public float north;

	public Rectangle(float west, float south, float east, float north)
	{
		this.west = west;
		this.south = south;
		this.east = east;
		this.north = north;
	}

	@Override
	public String toString()
	{
		return "Rectangle{" +
			"west=" + west +
			", south=" + south +
			", east=" + east +
			", north=" + north +
			'}';
	}
}
