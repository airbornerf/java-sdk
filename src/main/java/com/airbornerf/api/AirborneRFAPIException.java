package com.airbornerf.api;

public class AirborneRFAPIException extends Exception
{
    public AirborneRFAPIException(String message)
    {
        super(message);
    }
}
