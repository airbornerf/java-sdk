package com.airbornerf.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import okhttp3.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ACJA2API extends API
{
    private final String authToken;
    private final ObjectMapper objectMapper;

    public ACJA2API(String endpoint, String authToken)
    {
        super(endpoint);
        this.authToken = authToken;
        this.objectMapper = new ObjectMapper();
    }

    public static class ConnectivityProvider
    {
        public short MCC;
        public short MNC;
        public String technology;
    }

    private void checkJsonResult(ObjectNode result) throws JsonProcessingException, AirborneRFAPIException
    {
        if(!result.has("success"))
            throw new AirborneRFAPIException("API Request failed: " + this.objectMapper.writeValueAsString(result));

        boolean success = result.get("success").asBoolean();
        if(!success)
        {
            if(result.has("errorMessage"))
                throw new AirborneRFAPIException("API Request failed: " + result.get("errorMessage").asText());

            throw new AirborneRFAPIException("API Request failed");
        }

        // success!
    }

    /**
     * getVolume API call.
     *
     * @param serviceLevel
     * @return The taskId of the spawned background process on the server
     * @throws IOException
     * @throws AirborneRFAPIException If the API request was unsuccessful, containing the error message.
     */
    public long getVolume(String kind, String serviceLevel, ConnectivityProvider[] connectivityProviders,
                          String format, double lat1, double lon1, double lat2, double lon2) throws IOException, AirborneRFAPIException
    {
        // Build the request body JSON
        ObjectNode jsonBody = this.objectMapper.createObjectNode();
        jsonBody.put("kind", kind);
        jsonBody.put("serviceLevel", serviceLevel);
        jsonBody.put("format", format);
        for(ConnectivityProvider cp : connectivityProviders)
        {
            ArrayNode cpsJson = jsonBody.putArray("connectivityProvider");
            ObjectNode cpJson = cpsJson.addObject();
            cpJson.put("technology", cp.technology);
            cpJson.put("MCC", cp.MCC);
            cpJson.put("MNC", cp.MNC);
        }
        ObjectNode volumeJson = jsonBody.putObject("volume");
        volumeJson.put("id", "");
        volumeJson.put("type", "rectangle");
        ObjectNode geomJson = volumeJson.putObject("geometry");
        geomJson.put("lat1", lat1);
        geomJson.put("lon1", lon1);
        geomJson.put("lat2", lat2);
        geomJson.put("lon2", lon2);

        // Send the request
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(this.objectMapper.writeValueAsString(jsonBody), mediaType);
        Request request = new Request.Builder()
                .url(this.endpoint + "/acja/v2.00/getVolume")
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", this.authToken)
                .build();
        try(Response response = client.newCall(request).execute())
        {
            ObjectNode jsonResult = (ObjectNode) this.objectMapper.readTree(response.body().string());
            checkJsonResult(jsonResult);

            if(!jsonResult.has("taskId"))
                throw new AirborneRFAPIException("Malformed response: " + this.objectMapper.writeValueAsString(jsonResult));
            return jsonResult.get("taskId").asLong();
        }
    }

    public JsonNode getSubscriptions() throws IOException
    {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(this.endpoint + "/acja/v2.00/getVolume")
                .method("GET", null)
                .addHeader("Authorization", this.authToken)
                .build();
        try(Response response = client.newCall(request).execute())
        {
            return this.objectMapper.valueToTree(response.body().string());
        }
    }

    /**
     * pollTask. Investigate the state of a task.
     *
     * @param taskId The ID of the task to poll.
     * @param field The field to return in case of successful completion of the task.
     * @return Returns the requested field on success of the task. Returns null while still processing.
     * @throws IOException
     * @throws AirborneRFAPIException When the task failed.
     */
    public JsonNode pollTask(long taskId, String field) throws IOException, AirborneRFAPIException
    {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(this.endpoint + "/acja/v2.00/pollTask/" + taskId)
                .method("GET", null)
                .addHeader("Authorization", this.authToken)
                .build();
        try(Response response = client.newCall(request).execute())
        {
            ObjectNode jsonResult = (ObjectNode) this.objectMapper.readTree(response.body().string());
            if(!jsonResult.has("state"))
                throw new AirborneRFAPIException("Malformed response: " + this.objectMapper.writeValueAsString(jsonResult));

            String state = jsonResult.get("state").asText();
            if(state.equals("failed"))
                throw new AirborneRFAPIException("Task failed on the server");
            if(state.equals("aborted"))
                throw new AirborneRFAPIException("Task aborted on the server");
            if(state.equals("succeeded"))
            {
                if(!jsonResult.has(field))
                    throw new AirborneRFAPIException("Task succeeded but requested field not found in response: " + this.objectMapper.writeValueAsString(jsonResult));

                return jsonResult.get(field);
            }
            return null;
        }
    }

    /**
     * Download the given data ref to a new file.
     * @param ref
     * @param targetFile
     * @throws IOException
     * @throws AirborneRFAPIException
     */
    public void download(String ref, File targetFile) throws IOException, AirborneRFAPIException
    {
        ObjectNode jsonBody = this.objectMapper.createObjectNode();
        jsonBody.put("ref", ref);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(this.objectMapper.writeValueAsString(jsonBody), mediaType);
        Request request = new Request.Builder()
                .url(this.endpoint + "/acja/v2.00/download")
                .method("POST", body)
                .addHeader("Authorization", this.authToken)
                .build();
        try(Response response = client.newCall(request).execute())
        {
            InputStream is = response.body().byteStream();
            FileUtils.copyInputStreamToFile(is, targetFile);
        }
    }
}
