package com.airbornerf;

import com.airbornerf.api.ACJA2API;
import com.airbornerf.api.AirborneRFAPIException;
import com.airbornerf.api.Tile;
import com.airbornerf.api.Utils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.File;
import java.io.IOException;

public class Main
{
    public static void showcaseGetConnectivityGeoJSON(ACJA2API api) throws Exception
    {
        ACJA2API.ConnectivityProvider cp = new ACJA2API.ConnectivityProvider();
        cp.MCC = 1;
        cp.MNC = 1;
        cp.technology = "CELL_4G";

        // Start a "getVolume" request for connectivity (with GeoJSON result).
        String serviceLevel = "C2";
        long taskId = api.getVolume("connectivity", serviceLevel, new ACJA2API.ConnectivityProvider[]{cp},
                "geojson", 47.3, 16.2, 47.39, 16.29);
        System.out.println("getVolume success: taskId = " + taskId);

        // The background task now processes the request. Poll periodically until we get back a data ref.
        JsonNode dataRefs = null;
        do
        {
            Thread.sleep(2000);
            dataRefs = api.pollTask(taskId, "refs");
        } while(dataRefs == null);

        // Since we queried only 1 tile, we expect exactly 1 data ref.
        String dataRef = dataRefs.get(0).get("ref").asText();
        System.out.println("pollTask success: dataRef = " + dataRef);

        // We successfully got a data ref back. Download the data file.
        File dataFile = new File("downloaded-geojson-" + taskId);
        api.download(dataRef, dataFile);

        // Verify it is a valid GeoJSON file
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.readTree(dataFile);
    }

    public static void showcaseGetConnectivityPointcloud(ACJA2API api) throws Exception
    {
        ACJA2API.ConnectivityProvider cp = new ACJA2API.ConnectivityProvider();
        cp.MCC = 1;
        cp.MNC = 1;
        cp.technology = "CELL_4G";

        // Start a "getVolume" request for connectivity (with pointcloud result).
        String serviceLevel = "C2";
        long taskId = api.getVolume("connectivity", serviceLevel, new ACJA2API.ConnectivityProvider[]{cp},
                "pointcloud", 47.3, 16.2, 47.39, 16.29);
        System.out.println("getVolume success: taskId = " + taskId);

        // The background task now processes the request. Poll periodically until we get back a data ref.
        JsonNode dataRefs = null;
        do
        {
            Thread.sleep(2000);
            dataRefs = api.pollTask(taskId, "refs");
        } while(dataRefs == null);

        // Since we queried only 1 tile, we expect exactly 1 data ref.
        String dataRef = dataRefs.get(0).get("ref").asText();
        System.out.println("pollTask success: dataRef = " + dataRef);

        // We successfully got a data ref back. Download the data file.
        File dataFile = new File("downloaded-tile-" + taskId);
        api.download(dataRef, dataFile);

        // Now we can open the data file and verify its integrity
        Tile tile = new Tile(dataFile);
        if(!tile.validateChecksum())
            throw new Exception("Tile checksum verification failed!");

        // Now query a few locations and get the connectivity state there.

        // Convert a certain 3D coordinate (latitude, longitude, altitude) to tile coordinates (x, y, z):
        // Note: you need to make sure that you are within the boundaries of the tile. (check against tile.getHeader() fields)
        float latitude = 47.361f;
        float longitude = 16.2112f;
        float altitude = 590;
        Tile.PointXYZ xyz = tile.convertToTileCoordinates(latitude, longitude, altitude);

        int value = tile.getNode(xyz.x, xyz.y, xyz.z);
        switch(value)
        {
            case Integer.MIN_VALUE -> System.out.println("No connectivity information available at that coordinate");
            case 0 -> System.out.println("No connectivity for service level " + serviceLevel);
            case 1 -> System.out.println("Marginal connectivity for service level " + serviceLevel);
            case 2 -> System.out.println("Good connectivity for service level " + serviceLevel);
        }
    }

    public static void main(String[] args)
    {
        // Construct the API
        ACJA2API api = new ACJA2API(args[0], args[1]);

        try
        {
            showcaseGetConnectivityPointcloud(api);
            showcaseGetConnectivityGeoJSON(api);
        }
        catch (IOException | InterruptedException e)
        {
            throw new RuntimeException(e);
        }
        catch (AirborneRFAPIException e)
        {
            throw new RuntimeException(e);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
